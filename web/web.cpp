//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "web.h"

Web::WS::WS() {}

Web::WS::WS(int _in, std::string _mode, std::string _pubKey,
            std::string _privKey, std::string _caPubKey) {
  m_sec_server = NULL;
  m_server = NULL;
  m_mode = _mode; // TODO There is no mode anymore
  m_pubkey = _pubKey;
  m_privkey = _privKey;
  m_capubkey = _caPubKey;
  m_port = _in;
}

Web::WS::~WS() {
  logMessage("Killing Webserver...", "INFO");
  m_kill_thread = killServer();
  m_kill_thread->join();
  m_server_thread->join();
  delete (m_server_thread);
  delete (m_kill_thread);
  delete (m_server);
  delete (m_sec_server);
}

void Web::WS::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  if (_in == RUNNING)
    run();
  return;
}

void Web::WS::init() { return; }

void Web::WS::pause() { return; }

void Web::WS::run() {
  m_server_thread = startServer();
  return;
}

std::thread *Web::WS::startServer() {
  std::thread *server_thread;
  if (m_pubkey.length() != 0 && m_privkey.length() != 0) {
    m_sec_server = new SimpleWeb::Server<SimpleWeb::HTTPS>(m_pubkey, m_privkey,
                                                           m_capubkey);
    m_sec_server->config.port = m_port;
    m_sec_server->resource["^/json$"]["POST"] =
        [this](std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTPS>::Response>
                   response,
               std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTPS>::Request>
                   request) {
          try {
            boost::property_tree::ptree pt;
            read_json(request->content, pt);
            std::stringstream oss;
            write_json(oss, pt);
            API *api = new API(oss.str());
            std::string answer = api->evaluate();
            delete (api);
            *response << "HTTP/1.1 200 OK\r\n"
                      << "Content-Length: " << answer.length() << "\r\n\r\n"
                      << answer;
          } catch (const std::exception &e) {
            std::string answer = "API breach\n";
            *response << "HTTP/1.1 400 Bad Request\r\nContent-Length: "
                      << answer.length() << "\r\n\r\n"
                      << answer;
          }
        };
    server_thread = new std::thread([this]() { m_sec_server->start(); });
    std::this_thread::sleep_for(std::chrono::seconds(1));
  } else {
    logMessage("Spinning up Webserver on port: " + std::to_string(m_port),
               "DBG");
    m_server = new SimpleWeb::Server<SimpleWeb::HTTP>();
    m_server->config.port = m_port;

    m_server->resource["^/json$"]["POST"] =
        [this](std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTP>::Response>
                   response,
               std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTP>::Request>
                   request) {
          std::string answer;
          try {
            boost::property_tree::ptree pt;
            read_json(request->content, pt);
            std::stringstream oss;
            write_json(oss, pt);
            API *api = new API(oss.str());
            api->ForwardMessageSendRequest.connect(
                this, &Web::WS::AcceptMessageSendRequestForwarding);
            // api->ForwardNewTrustedEndpointAdded.connect(
            //    this, &Web::WS::AcceptNewTrustedEndpointAddedForwarding);
            api->ForwardNewTrustedChannelAdded.connect(
                this, &Web::WS::AcceptNewTrustedChannelAddedForwarding);
            answer = api->evaluate();
            api->ForwardMessageSendRequest.disconnect(this);
            // api->ForwardNewTrustedEndpointAdded.disconnect(this);
            api->ForwardNewTrustedChannelAdded.disconnect(this);
            delete (api);
            *response << "HTTP/1.1 200 OK\r\n"
                      << "Content-Length: " << answer.length() << "\r\n\r\n"
                      << answer;
          } catch (const std::exception &e) {
            answer = WS_API_BREACH_ANSWER;
            *response << "HTTP/1.1 400 Bad Request\r\nContent-Length: "
                      << answer.length() << "\r\n\r\n"
                      << answer;
          }
        };
    server_thread = new std::thread([this]() { m_server->start(); });
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  return server_thread;
}

std::thread *Web::WS::killServer() {
  std::thread *kill_thread = new std::thread([this]() {
    if (m_sec_server != NULL)
      m_sec_server->stop();
    if (m_server != NULL)
      m_server->stop();
  });
  return kill_thread;
}

const std::string Web::WS::identify() { return std::string("Web::WS"); }

void Web::WS::AcceptMessageSendRequestForwarding(
    std::tuple<std::string, std::vector<std::string>> _in) {
  MessageSendRequest.emit(_in);
  return;
}

void Web::WS::AcceptNewTrustedChannelAddedForwarding(std::string _in) {
  NewTrustedChannelAdded.emit(_in);
  return;
}

void Web::WS::AcceptNewTrustedEndpointAddedForwarding(std::string _in) {
  // NewTrustedEndpointAdded.emit(_in);
  return;
}

Web::WC::WC() {}

Web::WC::~WC() {}

void Web::WC::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  if (_in == RUNNING)
    run();
  return;
}

void Web::WC::init() { return; }

void Web::WC::pause() { return; }

void Web::WC::run() { return; }

const std::string Web::WC::identify() { return std::string("Web::WC"); }

Web::WSS::WSS() {}

Web::WSS::WSS(int _in, std::string _mode, std::string _pubKey,
            std::string _privKey, std::string _caPubKey) {
  m_sec_server = NULL;
  m_server = NULL;
  m_mode = _mode; // TODO There is no mode anymore
  m_pubkey = _pubKey;
  m_privkey = _privKey;
  m_capubkey = _caPubKey;
  m_port = _in;
}

Web::WSS::~WSS() {
  logMessage("Killing WebSocketServer...", "INFO");
  m_kill_thread = killServer();
  m_kill_thread->join();
  m_server_thread->join();
  delete (m_server_thread);
  delete (m_kill_thread);
  delete (m_server);
  delete (m_sec_server);
  conn = false;
}

void Web::WSS::AcceptRunModeOrder(int _in) {
  forwardRunModeOrder(_in);
  if (_in == RUNNING)
    run();
  return;
}

void Web::WSS::init() { return; }

void Web::WSS::pause() { return; }

void Web::WSS::run() {
  m_server_thread = startServer();
  return;
}

std::thread *Web::WSS::startServer() {
  logMessage("Spinning up Websocket server on port: " + std::to_string(m_port),
               "DBG");
  m_server = new SimpleWeb::SocketServer<SimpleWeb::WS>();
  m_server->config.port = 12355;
  auto &json = m_server->endpoint["^/json/?$"];
  json.on_message = [this](std::shared_ptr<SimpleWeb::SocketServer<SimpleWeb::WS>::Connection> connection, std::shared_ptr<SimpleWeb::SocketServer<SimpleWeb::WS>::InMessage> in_message) {
    API *api = new API(in_message->string());
    api->ForwardMessageSendRequest.connect(
                this, &Web::WSS::AcceptMessageSendRequestForwarding);
    api->ForwardNewTrustedChannelAdded.connect(
                this, &Web::WSS::AcceptNewTrustedChannelAddedForwarding);

    std::string answer = api->evaluate();
    api->ForwardMessageSendRequest.disconnect(this);
            // api->ForwardNewTrustedEndpointAdded.disconnect(this);
            api->ForwardNewTrustedChannelAdded.disconnect(this);

    delete (api);
    auto out_message = answer;

    // echo_all.get_connections() can also be used to solely receive connections on this endpoint
    //for(auto &a_connection : m_server->get_connections())
      connection->send(out_message);
  };
  json.on_open = [this](std::shared_ptr<SimpleWeb::SocketServer<SimpleWeb::WS>::Connection> connection) {
    conn = true;
  };


  std::thread *server_thread = new std::thread([this]() {
    m_server->start();
  });
  return server_thread;
}

std::thread *Web::WSS::killServer() {
  std::thread *kill_thread = new std::thread([this]() {
      if (m_sec_server != NULL){
	//for(auto &a_connection : m_sec_server->get_connections())
	//  a_connection->close();
        m_sec_server->stop();
      }	
      if (m_server != NULL){
	//for(auto &a_connection : m_server->get_connections())
        //  a_connection->close();
        m_server->stop();
      }
  });
  return kill_thread;
}

const std::string Web::WSS::identify() { return std::string("Web::WSS"); }

void Web::WSS::AcceptMessageSendRequestForwarding(
  std::tuple<std::string, std::vector<std::string>> _in) {
  MessageSendRequest.emit(_in);
  return;
}

void Web::WSS::AcceptNewTrustedChannelAddedForwarding(std::string _in) {
  NewTrustedChannelAdded.emit(_in);
  return;
}

void Web::WSS::AcceptNewTrustedEndpointAddedForwarding(std::string _in) {
  // NewTrustedEndpointAdded.emit(_in);
  return;
}

void Web::WSS::messageBroadcast(std::string _in){
  if(!conn) return;
  std::thread *broadcast_thread = new std::thread([this, _in]() {
      auto out_message = _in;

    // echo_all.get_connections() can also be used to solely receive connections on this endpoint
      for(auto &a_connection : m_server->get_connections())
      a_connection->send(out_message);

  });
  broadcast_thread->join();
  delete(broadcast_thread);
  return;
}

void Web::WSS::AcceptLogMessageToBroadcast(std::string _from, std::string _message, std::string _severity){
  std::time_t tt =
      std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  struct std::tm *ptm = std::localtime(&tt);
  std::stringstream timestream;
  timestream << std::put_time(ptm, "%c");
  std::string timestring = timestream.str();
  boost::property_tree::ptree message;
  boost::property_tree::ptree logmessage;
  logmessage.put("Time", timestring);
  logmessage.put("Module", _from);
  logmessage.put("Message", _message);
  logmessage.put("Severity", _severity);
  message.push_back(std::make_pair("LogMessage", logmessage));
  std::stringstream oss;
  write_json(oss, message);
  if(_severity != "DBG")
    messageBroadcast(oss.str());
  return;
}

void Web::WSS::AcceptDecryptedMessage(std::string _message, std::string _address){
  std::time_t tt =
      std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  struct std::tm *ptm = std::localtime(&tt);
  std::stringstream timestream;
  timestream << std::put_time(ptm, "%c");
  std::string timestring = timestream.str();
  boost::property_tree::ptree message;
  boost::property_tree::ptree decryptedmessage;
  decryptedmessage.put("Message", _message);
  decryptedmessage.put("Channel", _address);
  decryptedmessage.put("Time", timestring);
  message.push_back(std::make_pair("DecryptedMessage", decryptedmessage));
  std::stringstream oss;
  write_json(oss, message);
  messageBroadcast(oss.str());
  return;
}
