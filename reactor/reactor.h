//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef REACTOR_H
#define REACTOR_H

#include "common/helpers.h"
#include "qresdiotobject/qresdiotobject.h"

#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

class Reactor : public QResDIOTObject {
public:
  // Methods
  Reactor();
  ~Reactor();
  // Members
  // Signals
  // Slots
  virtual void AcceptRunModeOrder(int);

private:
  // Methods
  virtual const std::string identify();
  virtual void init();
  virtual void pause();
  virtual void run();
  std::string execute(std::string);
  // Members
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

#endif // REACTOR_H
