//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "reactor.h"

Reactor::Reactor() {}

Reactor::~Reactor() {}

const std::string Reactor::identify() { return std::string("Reactor"); }

void Reactor::init() { return; }

void Reactor::pause() { return; }

void Reactor::run() { return; }

void Reactor::AcceptRunModeOrder(int _in) { return; }

std::string execute(std::string _cmd) {
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(_cmd.c_str(), "r"),
                                                pclose);
  if (!pipe) {
    throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }
  return result;
}
