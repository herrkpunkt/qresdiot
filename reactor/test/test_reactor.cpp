//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "common/defaults.h"
#include "log/log.h"
#include "reactor/reactor.h"
#include <ctime>
#include <signal.h>
#include <thread>

bool killswitch = false;

sigslot::signal3<std::string, std::string, std::string> LogMessage;

const std::string identify() { return std::string("Test_Reactor_App"); }

void signal_handler(int s) {
  if (s == 2) {
    killswitch = true;
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), "Starting...", "INFO");
  while (!killswitch) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
