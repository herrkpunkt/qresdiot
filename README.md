# QResDIOT aka KUEdge #
- - - -
## Need to talk?
IOTA Discord: herrkpunkt#3501. Please ping me in #general or #casual. I'm not going to answer direct PMs.

## Project purpose:
This projects aims to deliver a gateway for IOT (Internet of Things) devices to the [IOTA](https://www.iota.org/) tangle. It utilizes only Open-Source software and is designed to be as modular as possible.
A simple [Web server](https://gitlab.com/eidheim/Simple-Web-Server) and a simple [WebSocket server](https://gitlab.com/eidheim/Simple-WebSocket-Server) is used to receive requests from any sensor service or data source. An API parses the requests and initiates the proper processing.Processing of a message to be sent basically consists of two steps:
1. Encrypt the message utilizing the [NTRU cryptosystem](https://en.wikipedia.org/wiki/NTRU) and prepare a [MAM](https://github.com/iotaledger/entangled/tree/develop/mam) bundle. This also means signing the message with the [Merkle tree signature scheme](https://en.wikipedia.org/wiki/Merkle_signature_scheme) that is widely used in [IOTA](https://www.iota.org/).
2. Send the bundle to the [IOTA](https://www.iota.org/) tangle using [CClient](https://github.com/iotaledger/entangled/tree/develop/cclient) for achieving semi-permanent storage.

The gateway also reads the stream from an [IRI](https://github.com/iotaledger/iri)-nodes ZMQ source. In this way, the device gets aware of messages from trusted *channels* and *endpoints*. The [bundle](https://docs.iota.org/docs/getting-started/0.1/transactions/bundles) hash is extracted from the ZMQ stream to be able to fetch the message [bundle](https://docs.iota.org/docs/getting-started/0.1/transactions/bundles). It then gets decrypted and the message can be processed further.

## How to use this project:
You have 2 options to use this project:
1. Run docker image from: https://hub.docker.com/r/kuhlmannmarkus/qresdiot (now also available with GUI: https://hub.docker.com/r/kuhlmannmarkus/qresdiot-gui)
2. Build the project with bazel and run the `TestApp` binary. This is available only on Linux.

This project is designed for communication. Therefore it is pretty much useless without some kind of partner. I want to suggest to find someone, who is also willing to try out the capabilities of this project. The two of you would have to exchange your NTRU public keys (can be obtained via the API: `GetNTRUPubKeys`) and the public key of your current channel (obtained via the API: `GetActiveChannelEndpointPubKeys`). Every participant then has to introduce the counterpart's public keys to their own instance. This can be achieved via the API calls: `AddTrustedNTRUPubKeys` and `AddTrustedChannelPubKeys` (These steps can also be achieved via the GUI). In this way you would be able to chat via the IOTA Tangle. Your messages are thought to be secure and NOT vulnerable to Shor's Algorithm and therefore safe against attacks with quantum computers. Read this [report](https://github.com/iotaledger/mam.c/blob/master/mam/rep.pdf) to learn more. Data immutability is NOT guaranteed by this project. You would need to trust the instances, you send messages to (since they are able to publish your contents where ever they want) and the instances, you receive messages from (the IOTA tangle delivers a mechanism of ordering transactions, which is probabilistic. Not every transactions is ordered, only the ones included in the tangle. This piece of software does NOT check the tangle history AT ALL).

## Network Selection:
You can now easily select the network to communicate with via environment variables. Please set those variables according to your choice:
* IRI_HOST: the IRI host, without protocol prefix. Default: nodes.comnet.thetangle.org
* IRI_PORT: the port, where your IRI host is reachable. Default: 443
* IRI_CA: this is still WIP. If the string is empty, QResDIOT will attempt a plain connection to the IRI host. If it is not empty, TLS will be attempted and checked against the Letsencrypt CA. Default: " "
* IRI_MWM: the minimum weight magnitude. Default: 10. For mainnet choose 14.
* ZMQ_URI: the URI of the ZMQ source with protocol prefix. Default: tcp://iotanode.kusys.de:5556.

## Dependencies:
* libboost-program-options-dev 
* libboost-system-dev 
* libboost-filesystem-dev 
* libboost-iostreams-dev 
* libzmqpp-dev
* bazel

All other dependencies will be fetched by bazel during the build process. This includes:
* [sigslot - C++ Signal/Slot Library](http://sigslot.sourceforge.net/)
* [Entangled](https://github.com/iotaledger/entangled)
* [Simple-Web-Server](https://gitlab.com/eidheim/Simple-Web-Server)
* [Simple-Websocket-Server](https://gitlab.com/eidheim/Simple-WebSocket-Server)
* [cpp-base64](https://github.com/ReneNyffenegger/cpp-base64)

This is the place to say thank you to everyone, who made my project possible:
* The Boost C++ team. [Boost C++](https://www.boost.org/)
* The ZeroMQ Team. [ZeroMQ](https://zeromq.org/)
* The Entangled team at the IOTA foundation. You guys are awesome! [Entangled](https://github.com/iotaledger/entangled)
* Sarah Thompson for the SigSlot library. [sigslot - C++ Signal/Slot Library](http://sigslot.sourceforge.net/)
* Ole Christian Eidheim for the Simple-Web-Server and Simple-WebSocket-Server libraries. Not simple at all, but simple to use. [Simple-Web-Server](https://gitlab.com/eidheim/Simple-Web-Server) [Simple-WebSocket-Server](https://gitlab.com/eidheim/Simple-WebSocket-Server)
* Rene Nyffenegger for the Base64 library. [cpp-base64](https://github.com/ReneNyffenegger/cpp-base64)
## Author:
Markus Kuhlmann
> Do NOT use this project in a productive environment.
> I'm working on this project on my weekends. I will not be able to improve anything in a sufficient timeframe and i will not be able to solve any issue, you might encounter.
> View this project as a proof of concept.
## Project status:

- [x] API component
- [x] Log component
- [x] ZMQ listener component
- [ ] ZMQ publisher component
- [x] Webserver component
- [x] Websocket server component
- [x] IOTA MAM API component
- [x] IOTA Tangle transport component
- [x] Reactor component (This component is supposed to react on incoming messages. Only one example is implemented, that lets you issue commands on the host system. This is NOWHERE used in the project and you should be very careful with it!)

## API commands: ##
**General remark:**
All the commands presented here, can be used to control your QResDIOT instance. The examples given reflect only the usage via the Webserver. It is also possible to use the Websocket server. To achieve this, you would just have to connect to the instances Websocket server (default port is 12355) and send the `data` part. The Websocket server will also answer your requests. Not all of the listed commands are already implemented. The one that are striked through may be implemented at a later point in time.

**Get hostname: (This will be implemented later. Right now it will return an error, as far as you don't have a TOR onion service properly configured.)**
* Command: *GetHostname*
* Parameters: NONE
* Return fields: Hostname
* cURL example: `curl -X POST --data '{"Command":"GetHostname"}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"GetHostname"}').RawContent`

**Get own NTRU public keys:**
* Command: *GetNTRUPubKeys*
* Parameters: NONE
* Return fields: NTRUPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetNTRUPubKeys"}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"GetNTRUPubKeys"}').RawContent`
 
**Get trusted NTRU public keys:**
* Command: *GetTrustedNTRUPubKeys*
* Parameters: NONE
* Return fields: NTRUPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetTrustedNTRUPubKeys"}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"GetTrustedNTRUPubKeys"}').RawContent`

**Add trusted NTRU public keys:**
* Command: *AddTrustedNTRUPubKeys*
* Parameters: NTRUPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"AddTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"AddTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}').RawContent`

**Remove trusted NTRU public keys:**
* Command: ~~*RemTrustedNTRUPubKeys*~~
* Parameters: NTRUPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"RemTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"RemTrustedNTRUPubKeys","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}').RawContent`

**Get trusted channel public keys:**
* Command: *GetTrustedChannelPubKeys*
* Parameters: NONE
* Return fields: ChannelPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetTrustedChannelPubKeys"}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"GetTrustedChannelPubKeys"}').RawContent`
 
**Add trusted channel public keys:**
* Command: *AddTrustedChannelPubKeys*
* Parameters: ChannelPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"AddTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"AddTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}').RawContent`
 
**Remove trusted channel public keys:**
* Command: ~~*RemTrustedChannelPubKeys*~~
* Parameters: ChannelPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"RemTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"RemTrustedChannelPubKeys","ChannelPubKeyArray":["CHANNEL1TRYTES","CHANNEL2TRYTES"]}').RawContent`
 
**Get trusted endpoint public keys:**
* Command: *GetTrustedEndpointPubKeys*
* Parameters: NONE
* Return fields: EndpointPubKeyArray
* cURL example: `curl -X POST --data '{"Command":"GetTrustedEndpointPubKeys"}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"GetTrustedEndpointPubKeys"}').RawContent`
 
**Add trusted endpoint public keys to existing channel:**
* Command: *AddTrustedEndpointPubKeys*
* Parameters: EndpointPubKeyArray, ChannelPubKey
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"AddTrustedEndpointPubKeys","ChannelPubKey":"CHANNELTRYTES","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"AddTrustedEndpointPubKeys","ChannelPubKey":"CHANNELTRYTES","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}').RawContent`

**Remove trusted endpoint public keys:**
* Command: ~~*RemTrustedEndpointPubKeys*~~
* Parameters: EndpointPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"RemTrustedEndpointPubKeys","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"RemTrustedEndpointPubKeys","EndpointPubKeyArray":["ENDPOINT1TRYTES","ENDPOINT2TRYTES"]}').RawContent`

**Get active channel and endpoint public keys:**
* Command: *GetActiveChannelEndpointPubKeys*
* Parameters: NONE
* Return fields: ChannelPubKey, EndpointPubKey
* cURL example: `curl -X POST --data '{"Command":"GetActiveChannelEndpointPubKeys"}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"GetActiveChannelEndpointPubKeys"}').RawContent`
 
**Send message: (Please keep in mind, that the entire `data` object must be a valid JSON. The Message body can be a string, that does not violate the JSON objects integrity, or a JSON object itself)**
* Command: *SendMessage*
* Parameters: MessageBody, NTRUPubKeyArray
* Return fields: NONE
* cURL example: `curl -X POST --data '{"Command":"SendMessage","MessageBody":"YOUR MESSAGE HERE","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}' localhost:12345/json`
* PS example: `(Invoke-WebRequest -UseBasicParsing http://localhost:12345/json -Method Post -Body '{"Command":"SendMessage","MessageBody":"YOUR MESSAGE HERE","NTRUPubKeyArray":["KEY1TRYTES","KEY2TRYTES"]}').RawContent`

## Bugs:

**There may be plenty of bugs and insufficiencies...**