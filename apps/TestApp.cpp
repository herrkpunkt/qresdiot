//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <signal.h>
#include <thread>

#include "log/log.h"
#include "mam/mam.h"
#include "tangle/tangle.h"
#include "web/web.h"
#include "zmq/zmq.h"
#include "reactor/reactor.h"

#include "common/defaults.h"
#include "common/helpers.h"

sigslot::signal3<std::string, std::string, std::string> LogMessage;
sigslot::signal1<int> SendRunModeOrder;
sigslot::signal1<std::vector<std::string>> SendZMQPatterns;

static volatile bool killswitch = false;

const std::string identify() { return std::string("TestApp"); }

void signal_handler(int s) {
  std::cout << "Caught Signal: " << s << std::endl;
  if (s == 2) {
    killswitch = true;
  }
}

int main(int argc, const char *argv[]) {
  std::string iri_host_s = TANGLE_NODE_HOST_DEF;
  char* iri_host_p;
  iri_host_p = getenv ("IRI_HOST");
  if (iri_host_p!=NULL){
    iri_host_s = std::string(iri_host_p);
    std::cout << "Host: " << iri_host_s << std::endl;
  }  
  int iri_port_s = TANGLE_NODE_PORT_DEF;
  char* iri_port_p;
  iri_port_p = getenv ("IRI_PORT");
  if (iri_port_p!=NULL){
    iri_port_s = std::stoi(std::string(iri_port_p));
    std::cout << "Port: " << iri_port_s << std::endl;
  }
  int iri_mwm_s = TANGLE_NODE_POW_MWM_DEF;
  char* iri_mwm_p;
  iri_mwm_p = getenv ("IRI_MWM");
  if (iri_mwm_p!=NULL)
    iri_mwm_s = std::stoi(std::string(iri_mwm_p));
  std::string iri_CA_s = " ";
  char* iri_CA_p;
  iri_CA_p = getenv ("IRI_CA");
  if (iri_CA_p!=NULL)
    iri_CA_s = std::string(iri_CA_p);
  bool iri_rempow_s = TANGLE_NODE_REMOTE_POW_DEF;
  char* iri_rempow_p;
  iri_rempow_p = getenv ("IRI_REMPOW");
  if (iri_rempow_p!=NULL)
    iri_rempow_s = boost::lexical_cast<bool>(std::string(iri_CA_p));
  std::string zmq_s = ZMQ_REC_TANGLE_URI_DEF;
  char* zmq_p;
  zmq_p = getenv ("ZMQ_URI");
  if (zmq_p!=NULL)
    zmq_s = std::string(zmq_p);
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  try {
    boost::program_options::options_description desc("Available options");
    desc.add_options()("help", "produce help message");
    boost::program_options::variables_map vm;
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }
  } catch (std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return 1;
  } catch (...) {
    std::cerr << "Exception of unknown type!\n";
  }
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), std::string(GPL), "INFO");
  LogMessage.emit(identify(), "Starting...", "INFO");
  MAM *mam = new MAM();
  mam->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(mam),
                           &QResDIOTObject::AcceptRunModeOrder);
  Reactor *reactor = new Reactor();
  reactor->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(reactor),
                           &QResDIOTObject::AcceptRunModeOrder);
  Web::WS *ws = new Web::WS(API_PORT_DEF, "test");
  ws->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(ws),
                           &QResDIOTObject::AcceptRunModeOrder);
  Web::WSS *wss = new Web::WSS(API_PORT_DEF_WSS, "test");
  wss->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(wss),
                           &QResDIOTObject::AcceptRunModeOrder);
  ZMQ::Receiver *rec = new ZMQ::Receiver(zmq_s);
  rec->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(rec),
                           &QResDIOTObject::AcceptRunModeOrder);
  SendZMQPatterns.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  mam->NewTrustedChannelAdded.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  Tangle *tangle = new Tangle(
      iri_host_s, iri_port_s, iri_rempow_s,
      TANGLE_NODE_GTTA_DEPTH_DEF, iri_mwm_s, iri_CA_s);
  mam->NewTrustedChannelAdded.connect(tangle, &Tangle::AcceptChannelFetchRequest);
  tangle->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(tangle),
                           &QResDIOTObject::AcceptRunModeOrder);
  ws->MessageSendRequest.connect(mam, &MAM::AcceptData);
  ws->NewTrustedChannelAdded.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  ws->NewTrustedChannelAdded.connect(tangle, &Tangle::AcceptChannelFetchRequest);
  wss->MessageSendRequest.connect(mam, &MAM::AcceptData);
  wss->NewTrustedChannelAdded.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  wss->NewTrustedChannelAdded.connect(tangle, &Tangle::AcceptChannelFetchRequest);
  // ws->NewTrustedEndpointAdded.connect(
  //    rec, &ZMQ::Receiver::AcceptPatterns);
  mam->PayloadDecryptedFromAddress.connect(wss, &Web::WSS::AcceptDecryptedMessage);
  mam->PayloadEncrypted.connect(tangle, &Tangle::AcceptSendRequest);
  rec->FoundPatternMatch.connect(tangle, &Tangle::AcceptFetchRequest);
  tangle->BundleFetched.connect(mam, &MAM::AcceptBundleToDecrypt);
  tangle->BundleFetchedFromAddress.connect(mam, &MAM::AcceptBundleToDecrypt);
  rec->LogMessage.connect(wss, &Web::WSS::AcceptLogMessageToBroadcast);
  tangle->LogMessage.connect(wss, &Web::WSS::AcceptLogMessageToBroadcast);
  ws->LogMessage.connect(wss, &Web::WSS::AcceptLogMessageToBroadcast);
  wss->LogMessage.connect(wss, &Web::WSS::AcceptLogMessageToBroadcast);
  mam->LogMessage.connect(wss, &Web::WSS::AcceptLogMessageToBroadcast);
  SendRunModeOrder.emit(2);
  LogMessage.emit(identify(), "Started", "INFO");
  while (!killswitch) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  LogMessage.emit(identify(), "Shutting down...", "INFO");
  SendRunModeOrder.emit(0);
  std::this_thread::sleep_for(std::chrono::seconds(1));
  LogMessage.emit(identify(), "Finished.", "INFO");
  rec->LogMessage.disconnect(wss);
  tangle->LogMessage.disconnect(wss);
  ws->LogMessage.disconnect(wss);
  wss->LogMessage.disconnect(wss);
  mam->LogMessage.disconnect(wss);
  ws->NewTrustedChannelAdded.disconnect(rec);
  wss->NewTrustedChannelAdded.disconnect(rec);
  ws->NewTrustedChannelAdded.disconnect(tangle);
  wss->NewTrustedChannelAdded.disconnect(tangle);
  tangle->BundleFetched.disconnect(mam);
  tangle->BundleFetchedFromAddress.disconnect(mam);
  rec->FoundPatternMatch.disconnect(tangle);
  mam->PayloadEncrypted.disconnect(tangle);
  mam->PayloadDecryptedFromAddress.disconnect(wss);
  ws->MessageSendRequest.disconnect(mam);
  wss->MessageSendRequest.disconnect(mam);
  SendRunModeOrder.disconnect(rec);
  SendZMQPatterns.disconnect(rec);
  mam->NewTrustedChannelAdded.disconnect(rec);
  mam->NewTrustedChannelAdded.disconnect(tangle);
  rec->LogMessage.disconnect(log);
  delete (rec);
  reactor->LogMessage.disconnect(log);
  SendRunModeOrder.disconnect(reactor);
  delete(reactor);
  SendRunModeOrder.disconnect(ws);
  ws->LogMessage.disconnect(log);
  delete (ws);
  SendRunModeOrder.disconnect(wss);
  wss->LogMessage.disconnect(log);
  delete (wss);
  mam->LogMessage.disconnect(log);
  SendRunModeOrder.disconnect(mam);
  delete (mam);
  tangle->LogMessage.disconnect(log);
  SendRunModeOrder.disconnect(tangle);
  delete (tangle);
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
