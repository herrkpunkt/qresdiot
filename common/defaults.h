//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#ifndef DEFAULT_H
#define DEFAULT_H

#define QRESDIOT_API_VERSION "0.1"
#define QRESDIOT_API_VERSION_DESCRIPTOR "API version"
#define APP_NAME "QResDIOT"
#define APP_NAME_DESCRIPTOR "App name"

#define TANGLE_NODE_HOST_DEF "nodes.comnet.thetangle.org"
#define TANGLE_NODE_PORT_DEF 443
#define TANGLE_NODE_REMOTE_POW_DEF false
#define TANGLE_NODE_GTTA_DEPTH_DEF 1
#define TANGLE_NODE_POW_MWM_DEF 10
#define API_PORT_DEF 12345
#define API_PORT_DEF_WSS 12355
#define ZMQ_PUB_LOG_PORT_DEF 10235
#define ZMQ_PUB_NEIGHBORCOMM_PORT_DEF 10236
#define ZMQ_REC_TANGLE_URI_DEF "tcp://iotanode.kusys.de:5556"
#define ZMQ_REC_TANGLE_TX_TOPIC_DEF "tx "

#define WS_API_BREACH_ANSWER "{\"Answer\": \"Malformed request\"}"
#define API_CMD_STR_IDENTIFIER "Command"
#define API_CMD_GET_HOSTNAME "GetHostname"
#define API_CMD_GET_OWN_NTRU_PUBLIC_KEYS "GetNTRUPubKeys"
#define API_CMD_GET_TRUSTED_NTRU_PUBLIC_KEYS "GetTrustedNTRUPubKeys"
#define API_CMD_ADD_TRUSTED_NTRU_PUBLIC_KEYS "AddTrustedNTRUPubKeys"
#define API_CMD_REMOVE_TRUSTED_NTRU_PUBLIC_KEYS "RemTrustedNTRUPubKeys"
#define API_CMD_GET_TRUSTED_CHANNEL_PUBLIC_KEYS "GetTrustedChannelPubKeys"
#define API_CMD_GET_TRUSTED_ENDPOINT_PUBLIC_KEYS "GetTrustedEndpointPubKeys"
#define API_CMD_ADD_TRUSTED_CHANNEL_PUBLIC_KEYS "AddTrustedChannelPubKeys"
#define API_CMD_REMOVE_TRUSTED_CHANNEL_PUBLIC_KEYS "RemTrustedChannelPubKeys"
#define API_CMD_ADD_TRUSTED_ENDPOINT_PUBLIC_KEYS "AddTrustedEndpointPubKeys"
#define API_CMD_REMOVE_TRUSTED_ENDPOINT_PUBLIC_KEYS "RemTrustedEndpointPubKeys"
#define API_CMD_GET_ACTIVE_CHANNEL_ENDPOINT_PUBLIC_KEYS                        \
  "GetActiveChannelEndpointPubKeys"
#define API_CMD_SEND_MESSAGE "SendMessage"
#define API_TEST_GET_HOSTNAME_JSON "{\"Command\": \"GetHostname\"}"
#define API_TEST_GET_OWN_NTRU_PUBLIC_KEYS_JSON                                 \
  "{\"Command\": \"GetNTRUPubKeys\"}"
#define API_TEST_GET_TRUSTED_NTRU_PUBLIC_KEYS_JSON                             \
  "{\"Command\": \"GetTrustedNTRUPubKeys\"}"
#define API_TEST_GET_TRUSTED_CHANNEL_PUBLIC_KEYS_JSON                          \
  "{\"Command\": \"GetTrustedChannelPubKeys\"}"
#define API_TEST_GET_TRUSTED_ENDPOINT_PUBLIC_KEYS_JSON                         \
  "{\"Command\": \"GetTrustedEndpointPubKeys\"}"
#define API_TEST_ADD_TRUSTED_CHANNEL_PUBLIC_KEYS_JSON                          \
  "{\"Command\": \"AddTrustedChannelPubKeys\",\"ChannelPubKeyArray\": "        \
  "[\"CHANNEL1TRYTES\",\"CHANNEL2TRYTES\"]}"
#define API_TEST_ADD_TRUSTED_ENDPOINT_PUBLIC_KEYS_JSON                         \
  "{\"Command\": \"AddTrustedEndpointPubKeys\",\"EndpointPubKeyArray\": "      \
  "[\"ENDPOINT1TRYTES\",\"ENDPOINT2TRYTES\"]}"
#define API_TEST_ADD_TRUSTED_NTRU_PUBLIC_KEYS_JSON                             \
  "{\"Command\": \"AddTrustedNTRUPubKeys\",\"NTRUPubKeyArray\": "              \
  "[\"NTRUPubKey1TRYTES\",\"NTRUPubKey2TRYTES\"]}"
#define API_TEST_GET_ACTIVE_CHANNEL_ENDPOINT_PUBLIC_KEYS_JSON                  \
  "{\"Command\": \"GetActiveChannelEndpointPubKeys\"}"
#define API_TEST_UNKNOWN_COMMAND_JSON "{\"Command\": \"SomethingUnknown\"}"
#define API_CHANNEL_PUBLIC_KEY_ARRAY_DESCRIPTOR "ChannelPubKeyArray"
#define API_ENDPOINT_PUBLIC_KEY_ARRAY_DESCRIPTOR "EndpointPubKeyArray"
#define API_CHANNEL_PUBLIC_KEY_DESCRIPTOR "ChannelPubKey"
#define API_ENDPOINT_PUBLIC_KEY_DESCRIPTOR "EndpointPubKey"
#define API_NTRU_PUBLIC_KEY_ARRAY_DESCRIPTOR "NTRUPubKeyArray"
#define API_MESSAGE_BODY_DESCRIPTOR "MessageBody"
#define API_STATUS_DESCRIPTOR "Status"
#define API_ANSWER_DESCRIPTOR "Answer"
#define API_REQUEST_DESCRIPTOR "Request"
#define API_HOSTNAME_DESCRIPTOR "Hostname"
#define API_ANSWER_SUCCESS_DESCRIPTOR "Successful"
#define API_ANSWER_FAIL_DESCRIPTOR "Failed"
#define API_ANSWER_UNKNOWN_COMMAND "Unknown command"
#define API_ANSWER_COMMAND_NOT_AVAILABLE "Command not available"
#define TOR_HOSTNAME_FILE_PATH "/var/lib/tor/hidden_qresdiot_service/hostname"

#define MAM_CHANNEL_PUBLIC_KEY_LENGTH 81
#define MAM_ENDPOINT_PUBLIC_KEY_LENGTH 81
#define MAM_NTRU_PUBLIC_KEY_LENGTH 3072
#define MAM_EMPTY_SEED                                                         \
  "99999999999999999999999999999999999999999999999999999999999999999999999999" \
  "9999999"
#define MAM_CH_EP_DEPTH_DEF 3

#define IOTA_TRYTE_ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ9"

#define GPL "QResDIOT Copyright (C) 2020  Markus Kuhlmann\nThis program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions."

#endif // DEFAULT_H
