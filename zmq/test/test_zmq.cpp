//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "common/defaults.h"
#include "log/log.h"
#include "zmq/zmq.h"
#include <ctime>
#include <signal.h>
#include <thread>

bool killswitch = false;

sigslot::signal3<std::string, std::string, std::string> LogMessage;
sigslot::signal1<std::string> SendZMQPatterns;
sigslot::signal1<int> SendRunModeOrder;
sigslot::signal1<std::string> SendZMQMessage;

const std::string identify() { return std::string("Test_ZMQ_App"); }

void signal_handler(int s) {
  if (s == 2) {
    killswitch = true;
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  ZMQ::Receiver *rec = new ZMQ::Receiver("tcp://multi.commatrix.de:5556");
  rec->LogMessage.connect(log, &Log::AcceptLogMessage);
  SendZMQPatterns.connect(rec, &ZMQ::Receiver::AcceptPatterns);
  SendRunModeOrder.connect(dynamic_cast<QResDIOTObject *>(rec),
                           &QResDIOTObject::AcceptRunModeOrder);
  LogMessage.emit(identify(), "Starting...", "INFO");
  SendZMQPatterns.emit("tx ");
  SendRunModeOrder.emit(2);
  // SendZMQPatterns.emit("SNOOPY9TWO999999999999999999999999999999999999999999999"
  //                     "99999999999999999999999999");
  while (!killswitch) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  SendRunModeOrder.emit(0);
  SendZMQPatterns.disconnect(rec);
  SendRunModeOrder.disconnect(rec);
  rec->LogMessage.disconnect(log);
  delete (rec);
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
