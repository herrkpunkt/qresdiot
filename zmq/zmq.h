//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef ZMQ_H
#define ZMQ_H

#include "common/defaults.h"
#include "common/helpers.h"
#include "qresdiotobject/qresdiotobject.h"

#include <zmqpp/zmqpp.hpp>

#include <algorithm>
#include <mutex>

namespace ZMQ {

struct iotatx {
  std::string hash;
  std::string adress;
  int value;
  std::string obsolete_tag;
  int timestamp;
  int current_index;
  int last_index;
  std::string bundle_hash;
  std::string tx_trunk;
  std::string tx_branch;
  int arrival_time;
  std::string tag;
};

class Publisher : public QResDIOTObject {
public:
  // Methods
  Publisher();
  Publisher(int);
  ~Publisher();
  // Members
  // Signals
  // Slots
  virtual void AcceptRunModeOrder(int);
  void AcceptData(std::string);

private:
  // Methods
  virtual const std::string identify();
  virtual void init();
  virtual void pause();
  virtual void run();
  virtual const std::vector<std::string> getConfigAttributes();
  // Members
  zmqpp::socket *m_socket;
  int m_port;
  std::mutex mtx;
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

class Receiver : public QResDIOTObject {
public:
  // Methods
  Receiver();
  Receiver(std::string);
  ~Receiver();
  void listen();
  // Members
  // Signals
  sigslot::signal1<std::string> FoundPatternMatch;
  // Slots
  virtual void AcceptRunModeOrder(int);
  void AcceptPatterns(std::vector<std::string>);
  void AcceptPatterns(std::string);

private:
  // Methods
  virtual const std::string identify();
  virtual void init();
  virtual void pause();
  virtual void run();
  virtual const std::vector<std::string> getConfigAttributes();
  // Members
  std::vector<std::string> m_patterns;
  std::vector<std::string> m_known_bundles;
  std::string m_endpoint;
  std::thread *m_listen_thread;
  std::vector<std::tuple<std::string, unsigned int>> m_bundle_watch;
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

} // namespace ZMQ

#endif // ZMQ_H
