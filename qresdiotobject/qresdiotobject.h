//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef QRESDIOTOBJECT_H
#define QRESDIOTOBJECT_H

#include "sigslot.h"
#include <ctime>
#include <string>
#include <vector>

enum STATE { NONE = 0, READY = 1, RUNNING = 2, FAILURE = 3 };

class QResDIOTObject : public sigslot::has_slots<> {
public:
  // Methods
  QResDIOTObject();
  virtual ~QResDIOTObject() = 0;
  // Members
  // Signals
  sigslot::signal3<std::string, std::string, std::string> LogMessage;
  sigslot::signal1<int> ForwardRunModeOrder;
  sigslot::signal1<int> PublishRunMode;
  // Slots
  void AcceptLogMessagePassthrough(std::string, std::string, std::string);
  virtual void AcceptRunModeOrder(int);

private:
  // Methods
  virtual void init();
  virtual void pause();
  virtual void run();
  virtual const std::vector<std::string> getConfigAttributes();
  virtual const std::string identify();
  // Members
  // Signals
  // Slots
protected:
  // Methods
  void logMessage(std::string, std::string);
  virtual void forwardRunModeOrder(int);
  void connectChild(QResDIOTObject *);
  void disconnectChild(QResDIOTObject *);
  // Members
  clock_t m_timeofcreation;
  STATE m_state;
  std::vector<QResDIOTObject *> m_children;
  // Signals
  // Slots
};

#endif // QRESDIOTOBJECT_H
