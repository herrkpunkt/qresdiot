//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "log/log.h"
#include <ctime>
#include <signal.h>
#include <thread>

sigslot::signal3<std::string, std::string, std::string> LogMessage;

const std::string identify() { return std::string("Test_Log_App"); }

void produceMessages(int _threadnumber) {
  unsigned int i = 0;
  while (i < 100) {
    LogMessage.emit(identify(),
                    "Concurrent message from thread " +
                        std::to_string(_threadnumber),
                    "INFO");
    i++;
  }
}

void signal_handler(int s) {
  if (s == 2) {
    exit(-1);
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), "Starting...", "INFO");
  LogMessage.emit(identify(), "This is an INFO message.", "INFO");
  LogMessage.emit(identify(), "This is an ERROR message.", "ERR");
  LogMessage.emit(identify(), "This is an DEBUG message.", "DBG");
  LogMessage.emit(identify(), "This is a message of unknown severity.", "");
  LogMessage.emit(identify(), "Let me now produce concurrent messages...",
                  "INFO");
  std::thread first(produceMessages, 1);
  std::thread second(produceMessages, 2);
  std::thread third(produceMessages, 3);
  std::thread fourth(produceMessages, 4);
  std::thread fifth(produceMessages, 5);
  first.join();
  second.join();
  third.join();
  fourth.join();
  fifth.join();
  LogMessage.emit(identify(), "Concurrent message test done.", "INFO");
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
