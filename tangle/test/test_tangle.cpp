//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "common/defaults.h"
#include "log/log.h"
#include "tangle/tangle.h"
#include <ctime>
#include <signal.h>
#include <thread>

sigslot::signal3<std::string, std::string, std::string> LogMessage;
sigslot::signal1<int> RunModeOrder;
sigslot::signal1<std::string> GetBundle;

const std::string identify() { return std::string("Test_Tangle_App"); }

bool killswitch = false;

void signal_handler(int s) {
  if (s == 2) {
    // exit(-1);
    killswitch = true;
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), "Starting...", "INFO");
  Tangle *tangle = new Tangle(
      TANGLE_NODE_HOST_DEF, TANGLE_NODE_PORT_DEF, TANGLE_NODE_REMOTE_POW_DEF,
      TANGLE_NODE_GTTA_DEPTH_DEF, TANGLE_NODE_POW_MWM_DEF, "");
  tangle->LogMessage.connect(log, &Log::AcceptLogMessage);
  GetBundle.connect(tangle, &Tangle::AcceptFetchRequest);
  RunModeOrder.connect(dynamic_cast<QResDIOTObject *>(tangle),
                       &QResDIOTObject::AcceptRunModeOrder);
  RunModeOrder.emit(2);
  GetBundle.emit("VSQZEJWPXLPRKHDTPLDEJDYPHYGGLATNIB9OQEIBKFBUZHIWXLGTTZFKNFHKV"
                 "DWZF9GECIERQYUBENSRZ");
  while (!killswitch) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
  RunModeOrder.emit(0);
  RunModeOrder.disconnect(tangle);
  tangle->LogMessage.disconnect(log);
  GetBundle.disconnect(tangle);
  delete (tangle);
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
