//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "tangle.h"

find_transactions_req_t recv_example_req;

static char const *letsencrypt_root =
    "-----BEGIN CERTIFICATE-----\r\n"
    "MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw\r\n"
    "TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh\r\n"
    "cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4\r\n"
    "WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu\r\n"
    "ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY\r\n"
    "MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc\r\n"
    "h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+\r\n"
    "0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U\r\n"
    "A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW\r\n"
    "T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH\r\n"
    "B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC\r\n"
    "B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv\r\n"
    "KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn\r\n"
    "OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn\r\n"
    "jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw\r\n"
    "qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI\r\n"
    "rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV\r\n"
    "HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq\r\n"
    "hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL\r\n"
    "ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ\r\n"
    "3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK\r\n"
    "NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5\r\n"
    "ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur\r\n"
    "TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC\r\n"
    "jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc\r\n"
    "oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq\r\n"
    "4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA\r\n"
    "mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d\r\n"
    "emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=\r\n"
    "-----END CERTIFICATE-----\r\n";
static int idx_sort(void const *lhs, void const *rhs) {
  iota_transaction_t *_lhs = (iota_transaction_t *)lhs;
  iota_transaction_t *_rhs = (iota_transaction_t *)rhs;

  return (transaction_current_index(_lhs) < transaction_current_index(_rhs))
             ? -1
             : (transaction_current_index(_lhs) >
                transaction_current_index(_rhs));
}

static void
get_first_bundle_from_transactions(transaction_array_t *const transactions,
                                   bundle_transactions_t *const bundle) {
  iota_transaction_t *tail = NULL;
  iota_transaction_t *curr_tx = NULL;
  iota_transaction_t *prev = NULL;
  utarray_sort(transactions, idx_sort);
  tail = (iota_transaction_t *)utarray_eltptr(transactions, 0);
  bundle_transactions_add(bundle, tail);
  prev = tail;
  TX_OBJS_FOREACH(transactions, curr_tx) {
    if (transaction_current_index(curr_tx) ==
            (transaction_current_index(prev) + 1) &&
        (memcmp(transaction_hash(curr_tx), transaction_trunk(prev),
                FLEX_TRIT_SIZE_243) == 0)) {
      bundle_transactions_add(bundle, curr_tx);
      prev = curr_tx;
    }
  }
}

retcode_t get_bundle_transactions(iota_client_service_t *serv,
                                  flex_trit_t const *const bundle_hash,
                                  transaction_array_t **const out_tx_objs) {
  flex_trit_t bundle_hash_flex[FLEX_TRIT_SIZE_243];
  recv_example_req.approvees = NULL;
  recv_example_req.bundles = NULL;
  recv_example_req.tags = NULL;
  recv_example_req.addresses = NULL;
  flex_trits_from_trytes(bundle_hash_flex, NUM_TRITS_BUNDLE, bundle_hash,
                         NUM_TRITS_HASH, NUM_TRYTES_BUNDLE);
  hash243_queue_push(&recv_example_req.bundles, bundle_hash);
  retcode_t err = iota_client_find_transaction_objects(serv, &recv_example_req,
                                                       *out_tx_objs);
  if (err != RC_OK) {
     fprintf(stderr, "iota_client_find_transaction_objects failed with %d\n", err);
    return err;
  }
  return RC_OK;
}

retcode_t receive_bundle(iota_client_service_t *serv,
                         tryte_t const *const bundle_hash,
                         bundle_transactions_t *const bundle) {
  retcode_t ret = RC_ERROR;
  retcode_t bundlevalidityret = RC_ERROR;
  bundle_status_t bundlestate = BUNDLE_NOT_INITIALIZED;
  transaction_array_t *out_tx_objs = NULL;
  out_tx_objs = transaction_array_new();
  int iter = 0;
  while (ret != RC_OK || bundlestate != BUNDLE_VALID || bundlevalidityret != RC_OK || utarray_len(out_tx_objs) == 0) {
    if(iter == 4){
      printf("Could not fetch bundle in 5 tries\n");
      return RC_ERROR;
    }
    if(iter != 0)std::this_thread::sleep_for(std::chrono::seconds(1));
    utarray_clear(out_tx_objs);
    ret = get_bundle_transactions(serv, bundle_hash, &out_tx_objs);
    bundle_transactions_t *bundletest = NULL;
    bundle_transactions_new(&bundletest);
    if(utarray_len(out_tx_objs) != 0){
      get_first_bundle_from_transactions(out_tx_objs, bundletest);
      bundlevalidityret = bundle_validate(bundletest, &bundlestate);
    }
    bundle_transactions_free(&bundletest);
    iter++;
  }
  get_first_bundle_from_transactions(out_tx_objs, bundle);
  transaction_array_free(out_tx_objs);
  hash243_queue_free(&recv_example_req.bundles);
  recv_example_req.bundles = NULL;
  return ret;
}

Tangle::Tangle() {}

Tangle::~Tangle() {
  iota_client_core_destroy(&m_client_service);
  delete (m_receiver_thread);
  delete (m_sender_thread);
  delete(m_channel_fetch_thread);
}

Tangle::Tangle(std::string _host, int _port, bool _rempow, int _depth, int _mwm,
               std::string _cacert) {
  m_receiver_thread = NULL;
  m_sender_thread = NULL;
  m_host = _host;
  m_port = _port;
  m_rempow = _rempow;
  m_depth = _depth;
  m_mwm = _mwm;
  if (_cacert.length() != 0) {
    std::ifstream t(_cacert);
    std::string str((std::istreambuf_iterator<char>(t)),
                    std::istreambuf_iterator<char>());
    m_cacert = _cacert;
  }
  m_state = NONE;
  block = false;
}

void Tangle::AcceptRunModeOrder(int _in) {
  if (_in == RUNNING && m_state == NONE) {
    m_state = RUNNING;
    init();
    run();
  } else if (_in == RUNNING && m_state == READY) {
    m_state = RUNNING;
    run();
  } else if (_in == READY && m_state == NONE) {
    init();
    m_state = READY;
  } else if (_in == READY && m_state == RUNNING) {
    pause();
    m_state = READY;
  } else if (_in == NONE && m_state == RUNNING) {
    m_state = NONE;
    pause();
  } else if (_in == NONE && m_state == READY) {
    m_state = NONE;
  }
  forwardRunModeOrder(_in);
  return;
}

void Tangle::init() {
  if (m_cacert.length() != 0) {
    logMessage("Attempting TLS...", "DBG");
    m_client_service = iota_client_core_init(m_host.c_str(), m_port, letsencrypt_root);
  } else {
    logMessage("Attempting plain...", "DBG");
    m_client_service = iota_client_core_init(m_host.c_str(), m_port, NULL);
  }
  checkNode();
  return;
}

void Tangle::pause() {
  m_state = READY;
  m_receiver_thread->join();
  m_sender_thread->join();
  m_channel_fetch_thread->join();
  return;
}

void Tangle::run() {
  m_receiver_thread = startRetrieving();
  m_sender_thread = startSending();
  m_channel_fetch_thread = startFetchingChannels();
  return;
}

const std::string Tangle::identify() { return std::string("Tangle"); }

void Tangle::node_info() {
  // logMessage("HELLO","INFO");
  retcode_t ret = RC_ERROR;
  get_node_info_res_t *node_res = get_node_info_res_new();
  if (node_res == NULL) {
    printf("Error: OOM\n");
    return;
  }

  if ((ret = iota_client_get_node_info(m_client_service, node_res)) == RC_OK) {
    printf("appName %s \n", get_node_info_res_app_name(node_res));
    printf("appVersion %s \n", get_node_info_res_app_version(node_res));
    // logMessage(get_node_info_res_app_name(node_res),"INFO");
    printf("latestMilestone: ");
    flex_trit_print(node_res->latest_milestone, NUM_TRITS_HASH);
    printf("\n");

    printf("latestMilestoneIndex %u \n", node_res->latest_milestone_index);

    printf("latestSolidSubtangleMilestone: ");
    flex_trit_print(node_res->latest_solid_subtangle_milestone, NUM_TRITS_HASH);
    printf("\n");

    printf("latestSolidSubtangleMilestoneIndex %u \n",
           node_res->latest_solid_subtangle_milestone_index);
    printf("neighbors %d \n", node_res->neighbors);
    printf("packetsQueueSize %d \n", node_res->packets_queue_size);
    // printf("time %" PRIu64 " \n", node_res->time);
    printf("tips %d \n", node_res->tips);
    printf("transactionsToRequest %d \n", node_res->transactions_to_request);
  } else {
    printf("Error: %s", error_2_string(ret));
  }

  get_node_info_res_free(&node_res);

  return;
}

bool Tangle::sendBundle(bundle_transactions_t *_in) {
  logMessage("Preparing bundle...", "DBG");
  retcode_t ret = RC_OK;
  Kerl kerl;
  kerl_init(&kerl);
  logMessage("Finalizing bundle...", "DBG");
  bundle_finalize(_in, &kerl);
  transaction_array_t *out_tx_objs = transaction_array_new();
  hash8019_array_p raw_trytes = hash8019_array_new();
  iota_transaction_t *curr_tx = NULL;
  flex_trit_t trits_8019[FLEX_TRIT_SIZE_8019];

  BUNDLE_FOREACH(_in, curr_tx) {

    transaction_serialize_on_flex_trits(curr_tx, trits_8019);
    hash_array_push(raw_trytes, trits_8019);
  }
  logMessage("Sending bundle...", "DBG");
  ret = iota_client_send_trytes(m_client_service, raw_trytes, m_depth, m_mwm,
                                NULL, !m_rempow, out_tx_objs);
  transaction_array_free(out_tx_objs);
  hash_array_free(raw_trytes);
  if (ret == RC_OK) {
    logMessage("Bundle sent successfully!", "DBG");
    return true;
  } else {
    logMessage("Error sending bundle!", "ERR");
    return false;
  }
}

void Tangle::AcceptSendRequest(bundle_transactions_t *_in) {
  m_bundle_send_queue.push(_in);
  return;
}

void Tangle::AcceptFetchRequest(std::string _in) {
  m_bundle_fetch_queue.push(_in);
  return;
}

bool Tangle::checkNode() {
  retcode_t ret = RC_ERROR;
  get_node_info_res_t *node_res = get_node_info_res_new();
  ret = iota_client_get_node_info(m_client_service, node_res);
  get_node_info_res_free(&node_res);
  if (ret == RC_OK) {
    logMessage("Node check successful", "DBG");
    return true;
  } else {
    logMessage("Node check failed", "ERR");
    return false;
  }
}

void Tangle::AcceptFetchRequest(std::vector<std::string> _in) {
  for (unsigned int i = 0; i < _in.size(); i++) {
    m_bundle_fetch_queue.push(_in.at(i));
  }
  return;
}

void Tangle::AcceptPriorityFetchRequest(std::string _in) {
  m_bundle_priority_fetch_queue.push(_in);
  return;
}

void Tangle::AcceptPriorityFetchRequest(std::vector<std::string> _in) {
  for (unsigned int i = 0; i < _in.size(); i++) {
    m_bundle_priority_fetch_queue.push(_in.at(i));
  }
  return;
}

bundle_transactions_t *Tangle::retrieveBundle(std::string _in) {
  logMessage("Retrieving bundle " + _in + "...", "DBG");
  retcode_t ret = RC_ERROR;
  bundle_transactions_t *bundle = NULL;
  bundle_transactions_new(&bundle);
  ret = receive_bundle(m_client_service, (tryte_t *)_in.c_str(), bundle);
  if (ret != RC_OK) {
    logMessage("Fetching bundle failed", "ERR");
    bundle_transactions_free(&bundle);
    return NULL;
  } else {

    logMessage("Bundle fetched successfully", "DBG");
  }
  // iota_transaction_t *curr_tx = NULL;
  // unsigned int numberOfTxs = 0;
  // BUNDLE_FOREACH(bundle, curr_tx) {
  // uint64_t hereindex = transaction_current_index(curr_tx);
  // logMessage("Index: " + std::to_string(hereindex), "DBG");
  // flex_trit_t *trits_243 = transaction_hash(curr_tx);
  // free(trits_243);
  // numberOfTxs++;
  //}
  // logMessage("Bundle contains " + std::to_string(numberOfTxs), "DBG");
  return bundle;
}

std::thread *Tangle::startRetrieving() {
  std::thread *retriever = new std::thread([this]() {
    while (m_state == RUNNING) {
      if (m_bundle_priority_fetch_queue.size() != 0) {
        bundle_transactions_t *bundle =
            retrieveBundle(m_bundle_priority_fetch_queue.front());
        m_bundle_priority_fetch_queue.pop();
	if(!bundle){
	  logMessage("It appears we have lost a bundle!","ERR");
	  continue;
	}  
        BundleFetched.emit(bundle);
	iota_transaction_t *tx0 = bundle_at(bundle, 0);
	flex_trit_t *addr = transaction_address(tx0);
	//transaction_free(tx0);
	tryte_t addr_trytes[NUM_TRYTES_ADDRESS];
	//trits_to_trytes(addr, addr_trytes, NUM_TRITS_ADDRESS);
	flex_trits_to_trytes(addr_trytes, NUM_TRYTES_ADDRESS, addr, NUM_TRITS_ADDRESS, NUM_TRITS_ADDRESS);
	std::string addr_s = "";
	for(unsigned int i = 0; i < NUM_TRYTES_ADDRESS; i++){
	  addr_s += (char)addr_trytes[i];
	}
	logMessage("Bundle fetched from address: " + addr_s, "DBG");
	BundleFetchedFromAddress.emit(bundle, addr_s);
        continue;
      }
      if (m_bundle_fetch_queue.size() == 0 || block) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      }
      bundle_transactions_t *bundle =
          retrieveBundle(m_bundle_fetch_queue.front());
      m_bundle_fetch_queue.pop();
      if(!bundle){
	logMessage("It appears we have lost a bundle!","ERR");
	continue;
      }
      BundleFetched.emit(bundle);
      iota_transaction_t *tx0 = bundle_at(bundle, 0);
      flex_trit_t *addr = transaction_address(tx0);
      tryte_t addr_trytes[NUM_TRYTES_ADDRESS];
      flex_trits_to_trytes(addr_trytes, NUM_TRYTES_ADDRESS, addr, NUM_TRITS_ADDRESS, NUM_TRITS_ADDRESS);
      //trits_to_trytes(addr, addr_trytes, NUM_TRITS_ADDRESS);
      //transaction_free(tx0);
      std::string addr_s = "";
      for(unsigned int i = 0; i < NUM_TRYTES_ADDRESS; i++){
        addr_s += (char)addr_trytes[i];
      }
      if(addr_s == std::string(MAM_EMPTY_SEED)){
	addr_s = "";
	logMessage("Bundle seems invalid... skipping...", "ERR");
	continue;
	//iota_transaction_t *tx1 = bundle_at(bundle, 1);
	//flex_trit_t *addr1 = transaction_address(tx1);
        //tryte_t addr_trytes1[NUM_TRYTES_ADDRESS];
        //flex_trits_to_trytes(addr_trytes1, NUM_TRYTES_ADDRESS, addr1, NUM_TRITS_ADDRESS, NUM_TRITS_ADDRESS);
	//for(unsigned int i = 0; i < NUM_TRYTES_ADDRESS; i++){
        //  addr_s += (char)addr_trytes1[i];
        //}
      }	
      BundleFetchedFromAddress.emit(bundle, addr_s);
      logMessage("Bundle fetched from address: " + addr_s, "DBG");
      // bundle_transactions_free(&bundle);
    }
  });
  return retriever;
}

std::thread *Tangle::startSending() {
  std::thread *sender = new std::thread([this]() {
    while (m_state == RUNNING) {
      if (m_bundle_send_queue.size() == 0) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      }
      bool worked = sendBundle(m_bundle_send_queue.front());
      if (worked) {
	iota_transaction_t *tx0 = bundle_at(m_bundle_send_queue.front(), 0);
        flex_trit_t *addr = transaction_bundle(tx0);
        tryte_t addr_trytes[NUM_TRYTES_ADDRESS];
        //trits_to_trytes(addr, addr_trytes, NUM_TRITS_ADDRESS);                                                                                                                                                                        
        flex_trits_to_trytes(addr_trytes, NUM_TRYTES_ADDRESS, addr, NUM_TRITS_ADDRESS, NUM_TRITS_ADDRESS);
        std::string addr_s = "";
        for(unsigned int i = 0; i < NUM_TRYTES_ADDRESS; i++){
          addr_s += (char)addr_trytes[i];
        }
        bundle_transactions_free(&m_bundle_send_queue.front());
        m_bundle_send_queue.pop();
        BundleSent.emit(addr_s);
	logMessage("Bundle sent: " + addr_s, "INFO");
      }
    }
  });
  return sender;
}

std::vector<std::string> Tangle::getBundlesFromAdress(std::string _in) {
  logMessage("Getting bundles...", "DBG");
  std::vector<std::string> result;
  std::vector<std::pair<std::string, unsigned long long int>> temp;
  std::vector<unsigned long long int> timestamps;
  result.clear();
  timestamps.clear();
  temp.clear();
  find_transactions_req_t *recv_req = find_transactions_req_new();
  find_transactions_res_t *find_tx_res = find_transactions_res_new();
  flex_trit_t address_hash_flex[NUM_FLEX_TRITS_ADDRESS];
  transaction_array_t *out_tx_objs = transaction_array_new();
  flex_trits_from_trytes(address_hash_flex, NUM_TRITS_ADDRESS,
                         (tryte_t *)_in.c_str(), NUM_TRYTES_ADDRESS,
                         NUM_TRYTES_ADDRESS);
  // recv_example_req.approvees = NULL;
  // recv_example_req.bundles = NULL;
  // recv_example_req.tags = NULL;
  // recv_example_req.addresses = NULL;
  find_transactions_req_address_add(recv_req, address_hash_flex);
  // hash243_queue_push(&recv_req.addresses, address_hash_flex);
  retcode_t err = iota_client_find_transaction_objects(m_client_service,
                                                       recv_req, out_tx_objs);
  if (err != RC_OK) {
    logMessage("Failed to fetch transactions with " + std::string(error_2_string(err)), "ERR");
    //fprintf(stderr, "iota_client_find_transaction_objects failed with %s\n",
    //        error_2_string(err));
    //return result;
  }
  find_transactions_req_free(&recv_req);
  find_transactions_res_free(&find_tx_res);
  iota_transaction_t *curr_tx = NULL;
  TX_OBJS_FOREACH(out_tx_objs, curr_tx) {
    flex_trit_t *bundle_here = transaction_bundle(curr_tx);
    unsigned long long int ts = transaction_timestamp(curr_tx);
    tryte_t bundle_hash[NUM_TRYTES_BUNDLE];
    flex_trits_to_trytes(bundle_hash, NUM_TRYTES_BUNDLE, bundle_here,
                         NUM_TRITS_BUNDLE, NUM_TRITS_BUNDLE);
    std::string bundle_string;
    for (unsigned int i = 0; i < NUM_TRYTES_BUNDLE; i++) {
      bundle_string += (char)bundle_hash[i];
    }
    if (std::find(result.begin(), result.end(), bundle_string) == result.end()){
      result.push_back(bundle_string);
      //timestamps.push_back(ts);
      temp.push_back(std::make_pair(bundle_string,ts));
    }  
    logMessage("Bundle found: " + bundle_string + " with timestamp: " + std::to_string(ts), "DBG");
  }
  transaction_array_free(out_tx_objs);
  std::sort(temp.begin(), temp.end(), 
	    [](std::pair<std::string, unsigned long long int> const &a, std::pair<std::string, unsigned long long int> const &b) { return a.second < b.second; });
  DB *db = new DB();
  std::vector<std::string> alreadyfetched = db->getBundlesFromCHannel(_in);
  delete(db);
  result.clear();
  for(unsigned int i = 0; i < temp.size(); i++){
    if (std::find(alreadyfetched.begin(), alreadyfetched.end(), temp.at(i).first) == alreadyfetched.end())
      result.push_back(temp.at(i).first);
  }
  return result;
}

std::vector<std::string> Tangle::Do(std::string _in) {
  return getBundlesFromAdress(_in);
}

void Tangle::AcceptChannelFetchRequest(std::string _channel){
  //std::vector<std::string> bundles = getBundlesFromAdress(_channel);
  //AcceptPriorityFetchRequest(bundles);
  block = true;
  m_channel_fetch_queue.push(_channel);
  return;
}

std::thread *Tangle::startFetchingChannels() {
  logMessage("Now fetching channel...", "DBG");
  std::thread *channelfetcher = new std::thread([this]() {
    while (m_state == RUNNING) {
      if (m_channel_fetch_queue.size() == 0) {
	block = false;
        std::this_thread::sleep_for(std::chrono::seconds(1));
        continue;
      }
      std::vector<std::string> bundles = getBundlesFromAdress(m_channel_fetch_queue.front());
      m_channel_fetch_queue.pop();
      AcceptPriorityFetchRequest(bundles);
      block = false;
    }
  });
  return channelfetcher;
}
