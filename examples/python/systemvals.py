#
# Copyright (c) 2019 Markus Kuhlmann
#
# Refer to the LICENSE file for licensing information
#

# This example tool will gather a few system relevant metrics and push them as a JSON to the QResDIOT service.

import socket
import time
import json
import psutil
import requests

url = 'http://localhost:1234/json'

for x in range(1000):
  now = time.time()
  cpu = psutil.cpu_percent()
  mem = psutil.virtual_memory().percent
  swap = psutil.swap_memory().percent
  y = {"CPU usage": cpu,"Memory usage": mem,"Swap usage": swap,"Timestamp": now, "Hostname": socket.gethostname()}
  z = json.dumps(y)
  myobj = '{"Command":"SendMessage","MessageBody":' + z + ',"NTRUPubKeyArray":[]}'
  x = requests.post(url, data = myobj)
  print(x.text)
  time.sleep(30)
