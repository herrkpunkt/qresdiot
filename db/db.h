//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#ifndef DB_H
#define DB_H

#include <mutex>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

class DB{
public:
  // Methods
  DB();
  ~DB();
  void addBundleToChannel(std::string, std::string);
  std::vector<std::string> getBundlesFromCHannel(std::string);
  // Members
  // Signals
  // Slots
private:
  // Methods
  // Members
  std::mutex mtx;
  // Signals
  // Slots
protected:
  // Methods
  // Members
  // Signals
  // Slots
};

#endif // DB_H
