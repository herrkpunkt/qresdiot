//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//

#include "log/log.h"
#include "db/db.h"
#include <ctime>
#include <signal.h>
#include <thread>

sigslot::signal3<std::string, std::string, std::string> LogMessage;

const std::string identify() { return std::string("Test_DB_App"); }

void signal_handler(int s) {
  if (s == 2) {
    exit(-1);
  }
}

int main(int argc, const char *argv[]) {
  clock_t begin = clock();
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = signal_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);
  Log *log = new Log();
  DB *db = new DB();
  LogMessage.connect(log, &Log::AcceptLogMessage);
  LogMessage.emit(identify(), "Starting...", "INFO");
  clock_t end = clock();
  LogMessage.emit(
      identify(),
      "Finished in: " + std::to_string(double(end - begin) / CLOCKS_PER_SEC) +
          "s",
      "INFO");
  db->addBundleToChannel("test1","hallo");
  db->addBundleToChannel("test2","hallo");
  std::vector<std::string> res = db->getBundlesFromCHannel("hallo");
  for(unsigned int i = 0; i < res.size(); i++){
    LogMessage(identify(), "In file: " + res.at(i), "INFO");
  }
  delete(db);
  LogMessage.disconnect(log);
  delete (log);
  return 0;
}
