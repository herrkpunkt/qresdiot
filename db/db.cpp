//
// Copyright (c) 2019 Markus Kuhlmann
//
// Refer to the LICENSE file for licensing information
//
#include "db.h"

DB::DB() {}

DB::~DB() {}

void DB::addBundleToChannel(std::string _bundle, std::string _channel){
  std::string path = "storage/" + _channel + ".db";
  std::ofstream outfile;
  mtx.lock();
  outfile.open(path.c_str(), std::ios_base::app); // append instead of overwrite
  outfile << _bundle;
  outfile << std::endl;
  outfile.close();
  mtx.unlock();
  return;
}

std::vector<std::string> DB::getBundlesFromCHannel(std::string _channel){
  std::vector<std::string> result;
  result.clear();
  mtx.lock();
  std::string path = "storage/" + _channel + ".db";
  std::ifstream infile;
  infile.open(path.c_str());
  std::string line;
  while (std::getline(infile, line)){
    result.push_back(line);
  }
  infile.close();
  mtx.unlock();
  return result;
}
